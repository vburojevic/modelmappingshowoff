//
//  AppDelegate.h
//  ModelMappingShowoff
//
//  Created by Vedran Burojevic on 02/10/15.
//  Copyright © 2015 Infinum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

